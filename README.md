[![pipeline status](https://gitlab.com/ben-and-nico/fdi-ops2/badges/main/pipeline.svg)](https://gitlab.com/ben-and-nico/fdi-ops2/-/commits/ci/base-ci-implementation)
[![coverage report](https://gitlab.com/ben-and-nico/fdi-ops2/badges/main/coverage.svg)](https://gitlab.com/ben-and-nico/fdi-ops2/-/commits/ci/base-ci-implementation)

# pipeline-integration <!-- omit in toc -->
*grosdu_b – decorb_n*

Ce project consiste à créer la CI/CD du projet API Restful avec l'aide de Gitlab.

- [Mise en place des runners](#mise-en-place-des-runners)
  - [Runner de test](#runner-de-test)
  - [Runner de deploy](#runner-de-deploy)
- [Mise en place de la CI](#mise-en-place-de-la-ci)
  - [`build`](#build)
  - [`test`](#test)
  - [`clean`](#clean)
- [Mise en place de la CD](#mise-en-place-de-la-cd)
  - [`deliver`](#deliver)
  - [`deploy`](#deploy)

## Mise en place des runners
![Runner Schema](assets/Runner-OPS2.png)
### Runner de test
Ce runner est celui qu'on utilisera le plus pour la CI/CD ; sa création est là pour combler les lacunes des machines de l'ETNA (`npm install` prenant plus d'une heure, etc.).
|               |               |
|---------------|---------------|
| OS            | Debian Buster |
| vCPU          | 1             |
| RAM           | 3.5 G         |
| Gitlab Runner | Docker        |

Cette configuration est optimale pour le lancement de nos tests, ainsi que pour build/deliver notre code.
> Sur le schema, vous noterez la présence d'un [`MinIO storage`](https://min.io/) : c'est un *S3 compatible storage* que nous utilisons afin de bénéficier d'un [distributed cache](https://docs.gitlab.com/runner/configuration/speed_up_job_execution.html#use-a-distributed-cache) pour nos runners, et ainsi d'augmenter le nombre de `concurents` et de gagner du temps.

Ci-dessous, la configuration de ce runner : 
```toml
# max. 3 job at the same time 
concurrent = 3

[...]

[[runners]]
  name = "Azure Cloud Runner for pipeline-integration"
  url = "https://gitlab.com/"
  token = "$TOKEN"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    Type = "s3"
    Shared = false
    [runners.cache.s3]
      # setup minio storage
      ServerAddress = "minio:9000"
      AccessKey = "$MINIO_USER"
      SecretKey = "$MINIO_SECRET"
      BucketName = "$MINIO_BUCKET"
      Insecure = true
  [runners.docker]
    image = "ubuntu:latest"
    [...]
```
### Runner de deploy
Ce runner est uniquement utilisé pour le `deploy` de notre stack. Très lent, il n'est que très peu utilisé.

|               |               |
|---------------|---------------|
| OS            | Debian        |
| vCPU          | ?             |
| RAM           | ?             |
| Gitlab Runner | Shell         |

Une configuration simple qui permet d'utiliser le Docker engine installé en local. La configuration est classique, puisqu'il n'a besoin de lancer que des commandes en rapport avec `docker-compose`. 

## Mise en place de la CI
![CI Schema](assets/CI-OPS2.png)
### `build`
La phase de build comporte 2 jobs : 
- 1: nous créons une image Docker temporaire de notre database à des fins de tests, à partir de [ce Dockerfile](mongo/Dockerfile). cette image sera stockée dans la *container registery* de Gitlab pour nos tests.
- 2: nous installons nos dépendances `npm`, puis nous les stockons nos `nodes_modules` dans un cache qui sera, lui aussi, utilisé lors de nos tests.
### `test`
Nous allons lancer 3 jobs dans cette partie : 
- 1: on teste la connexion à notre database avec une simple commande mongo
- 2: on récupère notre cache pour lancer le `linter`
- 3: toujours en récupérant le cache, on lance nos tests unitaires. 

Cette dernière phase génère des *coverage reports* qui seront disponible en tant qu'*artifacts*, mais également directement depuis le site de Gitlab lors des merge requests, par exemple.
### `clean`
Cette phase va venir supprimer l'image temporaire de la DB, afin de pouvoir créer une image propre pour le déploiement. Cette phase vient volontairement après les tests afin de pouvoir pull cette image, et de reproduire nos tests.
## Mise en place de la CD
![CI Schema](assets/CD-OPS2.png)
### `deliver`
Ici, nous allons uniquement builder nos images Docker et les push dans la container registery, avec comme tag la branche actuelle.
### `deploy`
Enfin, on peut deploy notre infrastructure à l'aide de [docker-compose](docker-compose.yml) sur notre Runner de deploy.

> Ce stage n'est trigger qu'en cas de push sur la branche main, vu que nous n'avons pas de serveurs de tests.
