# FDI-DVJS2 - API <!-- omit in toc -->
*grosdu_b – decorb_n*

- [What is this?](#what-is-this)
- [Installation:](#installation)
  - [By using `docker`: *(recommended)*](#by-using-docker-recommended)
  - [By using `npm`:](#by-using-npm)
- [How to use it?](#how-to-use-it)
  - [By using `docker`: *(recommended)*](#by-using-docker-recommended-1)
  - [By using `npm`:](#by-using-npm-1)
- [Code](#code)

## What is this?

This is the backend part of this project. Mostly developed in [*TypeScript*](https://www.typescriptlang.org/), it implements the following features : 
- *Insane logs management*, by combining [`winston`](https://github.com/winstonjs/winston) and [`morgan`](https://www.npmjs.com/package/morgan) loggers.
- *Portability*, by using [`TypeORM`](https://typeorm.io/#/) rather than [`Mongoose`](https://mongoosejs.com/), which is limited to `MongoDB` databases.
- *Security*, by implementing a lot of methods to check the data, encrypt sensitive data using [`CryptoJS`](https://www.npmjs.com/package/crypto-js), or authentication & authorization using [`JsonWebToken`](https://jwt.io/).
- *Code quality*, in strict compliance with [`ESLint`](https://eslint.org/), and code documentation based on the [`JSDoc` standards](https://jsdoc.app/)
- *Easy to use*, by combining [`Docker`](https://www.docker.com/) containerization, and [`NodeJS`](https://nodejs.org/en/) dependencies management.

Built using the [`Express` framework](https://expressjs.com/), the usage of `typescript` on top of it ensure a really robust application.

## Installation:

You can use `Docker` or `npm` to install this project : 

### By using `docker`: *(recommended)*

```console
$ docker build . -t <your-desired-name>
```

### By using `npm`:

```console
$ npm install
```

> By using `npm`, you are responsable of the node version, and the compatibility of each dependecies with your system. Also, note that many packages will be installed without any isolation.

The API is almost ready to be launched ! Let's have a look at the configuration.

## How to use it?

> I strongly recommends you to use the `docker-compose` method, presented in main documentation, to launch this application.

The configuration is based on environment variables : 
- `DATABASE_PORT` (default: `27017`): the database port to use for db connection.
- `DATABASE_HOST` (default: `localhost`): the database host to use.
- `DATABASE_DB` (default: `mongo`): the database name to use.
- `DATABASE_USER` (default: `root`): the database user to use.
- `DATABASE_PASSWORD` (default: `password`): the database user password.
- `API_PORT` (default: `9000`): the port used by the API.

### By using `docker`: *(recommended)*

> You can add environments variables while running containers by adding the `-e` flag.
```console
$ docker run <your-image-name> -e API_PORT=9999 --env DATABASE_USER
```

### By using `npm`:

```console
$ npm start
```

## Code

All source code is located under the [`src`](./src) directory.