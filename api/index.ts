import app from "./src/express"

import { winstonLogger } from "./src/Config/Loggers"
import { createConnection } from "typeorm"
import dbConfig from "./src/Config/database"

const API_PORT = process.env["API_PORT"] || 9000

winstonLogger.info("Connecting to database...")
createConnection(dbConfig)
  .then( () => {
    app.listen(API_PORT, () => {
      winstonLogger.info("Successfully connected to database !")
      winstonLogger.info("Server is now running on port " + API_PORT)
    })
  })
  .catch((error) => {
    winstonLogger.error("Unable to connect to db : \n" + error)
    process.exit(1)
  })
