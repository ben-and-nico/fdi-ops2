import morgan, { StreamOptions } from "morgan"
import winstonLogger from "./winston"

/**
 *  Overide default logger to use Winston Logger
 */
const stream: StreamOptions = {
  write: (message) => winstonLogger.http(message),
}

/**
 *  Overide classic morgan log display and stream
 */
const morganLogger = morgan(
  ":method :url :status - :response-time ms",
  { 
    stream: stream
  }
)

export default morganLogger