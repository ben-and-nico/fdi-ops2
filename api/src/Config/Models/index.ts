import { WaifuModel } from "./WaifuModel"
import { AnimeModel } from "./AnimeModel"
import { TypeModel } from "./TypeModel"
import { UserModel } from "./UserModel"
export { WaifuModel, AnimeModel, TypeModel, UserModel }
