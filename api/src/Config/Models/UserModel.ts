import {
  Entity,
  Column,
  ObjectID,
  ObjectIdColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index
} from "typeorm"


/**
 *  Entity to describe Users in database
 */
@Entity("users")
export class UserModel {

  @ObjectIdColumn()
  _id!: ObjectID

  @Index({ unique: true })
  @Column()
  username!: string

  @Column()
  password!: string

  @CreateDateColumn()
  createdAt!: Date
  
  @UpdateDateColumn()
  updatedAt!: Date
}
