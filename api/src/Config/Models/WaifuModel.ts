import {
  Entity,
  Column,
  ObjectID,
  ObjectIdColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index
} from "typeorm"


/**
 *  [Entity to describe Waifu in database]
 */
@Entity("waifus")
export class WaifuModel {

  @ObjectIdColumn()
  _id!: ObjectID

  @Index({ unique: true })
  @Column()
  name!: string

  @Column({
    type: "int"
  })
  age!: number

  @Column()
  anime!: string

  @Column()
  image!: string

  @Column()
  owner!: string

  @CreateDateColumn()
  createdAt!: Date

  @UpdateDateColumn()
  updatedAt!: Date
}
