import {
  Entity,
  Column,
  ObjectID,
  ObjectIdColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index
} from "typeorm"


/**
 *  Entity to describe Anime in database
 */
@Entity("animes")
export class AnimeModel {

  @ObjectIdColumn()
  _id!: ObjectID

  @Index({ unique: true })
  @Column()
  name!: string

  @Column()
  type!: string

  @Column()
  image!: string

  @Column({
    type: "text"
  })
  description!: string

  @Column()
  owner!: string

  @CreateDateColumn()
  createdAt!: Date
  
  @UpdateDateColumn()
  updatedAt!: Date
}
