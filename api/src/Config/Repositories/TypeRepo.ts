import { DeleteResult, getMongoRepository } from "typeorm"

import { TypeModel } from "../Models"
import { ErrorManager } from "../Security"
import { getOneAnimeByName } from "./AnimeRepo"


/**
 * generate an error message if the type doesn't exists
 * @param name      the name of the type
 * @returns         an object with all informations to generate the error code
 */
function TypeNotFound( name: string ) {
  const errorManager = new ErrorManager
  return errorManager.notFoundError(
    "Type",
    name
  )
}

/**
 *  payload definition for POST and PUT methods
 */
export interface TypeSchema {
  name: string
  image: string
  description: string
  owner: string
}

/**
 *  async function to retrieve all types
 *  @return a promise with an array of TypeModel
 */
export const getAllTypes = async (): Promise<TypeModel[]> => {
  const typeRepository = getMongoRepository( TypeModel )
  return typeRepository.find()
}

/**
 *  async function to retrieve all types by an owner name
 *  @param  request name of the owner to find
 *  @return         a promise with an array of TypeModel
 */
 export const getAllTypesByOwner = async (
  request: string
): Promise<TypeModel[]> => {
  const typeRepository = getMongoRepository( TypeModel )
  return typeRepository.find({ 
    where: {
      owner: request 
    }
  })
}

/**
 *  async function to create a new type
 *  @param  request TypeSchema with data
 *  @return         a promise with the created type
 */
export const createType = async (
  request: TypeSchema
): Promise<TypeModel> => {

  const typeRepository = getMongoRepository( TypeModel )
  const type = new TypeModel()
  
  return typeRepository.save({
    ...type,
    ...request,
  })
}

/**
 *  async function to retrieve one type by its name
 *  @param  request     name of the type to find
 *  @return             a promise with a TypeModel
 */
export const getOneTypeByName = async (
  request: string
): Promise<TypeModel> => {
  
  const typeRepository = getMongoRepository( TypeModel )
  let type = null
  type = await typeRepository.findOne({ name: request })
    
  if ( !type )
  {
    throw TypeNotFound( request ) as Error
  }
  return type
}

/**
 *  async function to retrieve one type by a anime name
 *  @param  request     name of the type to find
 *  @return             a promise with a AnimeModel
 */
export const getOneTypeByAnime = async (
  request: string
): Promise<TypeModel> => {

  const anime = await getOneAnimeByName( request )
  return getOneTypeByName( anime.type )   
}

/**
 *  async function to update one type
 *  @param  request     name of the type to update
 *  @param  data        a TypeSchema with modifications
 *  @return             a promise with the updated TypeModel
 */
 export const updateOneType = async (
  request: string,
  data: TypeSchema
): Promise<TypeModel> => {
  
  const typeRepository = getMongoRepository( TypeModel )
  let type = null
  type = await typeRepository.findOne({ name: request })
    
  if ( !type )
  {
    throw TypeNotFound( request ) as Error
  }

  typeRepository.merge( type, data )
  return typeRepository.save( type )
}

/**
 *  async function to delete one type
 *  @param  request     name of the type to delete
 *  @return             DeleteResult with affected type
 */
export const deleteOneType = async (
  request: string
): Promise<DeleteResult> => {
  
  const typeRepository = getMongoRepository( TypeModel )
  const result = await typeRepository.delete({ name: request })

  if ( !result.affected || result.affected < 1 )
  {
    throw TypeNotFound( request ) as Error
  }
  return result
}
