import { DeleteResult, getMongoRepository } from "typeorm"

import { UserModel } from "../Models"
import { ErrorManager } from "../Security"


/**
 * generate an error message if the type doesn't exists
 * @param name      the name of the type
 * @returns         an object with all informations to generate the error code
 */
function UserNotFound( name: string ) {
  const errorManager = new ErrorManager
  return errorManager.notFoundError(
    "User",
    name
  )
}

/**
 *  payload definition for POST and PUT methods
 */
export interface UserSchema {
  username: string
  password: string
}

/**
 *  async function to create a new user
 *  @param  request UserSchema with data
 *  @return         a promise with the created user
 */
export const createUser = async (
  request: UserSchema
): Promise<UserModel> => {

  const userRepository = getMongoRepository( UserModel )
  const user = new UserModel()
  
  return userRepository.save({
    ...user,
    ...request,
  })
}

/**
 *  async function to retrieve one user by its name
 *  @param  request     username of the user to find
 *  @return             a promise with a UserModel
 */
export const getOneUserByUsername = async (
  request: string
): Promise<UserModel> => {

  const userRepository = getMongoRepository( UserModel )
  const user: UserModel | undefined = await userRepository.findOne({ username: request })
    
  if ( !user )
  {
    throw UserNotFound( request ) as Error
  }
  return user
}

/**
 *  async function to delete one user
 *  @param  request     name of the user to delete
 *  @return             DeleteResult with affected user
 */
export const deleteOneUser = async (
  request: string
): Promise<DeleteResult> => {

  const userRepository = getMongoRepository( UserModel )
  const result = await userRepository.delete({ username: request })

  if ( !result.affected || result.affected < 1 )
  {
    throw UserNotFound( request ) as Error
  }
  return result
}
