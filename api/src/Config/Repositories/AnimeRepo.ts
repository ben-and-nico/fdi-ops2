import { DeleteResult, getMongoRepository } from "typeorm"

import { AnimeModel } from "../Models"
import { ErrorManager } from "../Security"
import { getOneWaifuByName } from "./WaifuRepo"


/**
 * generate an error message if the anime doesn't exists
 * @param name      the name of the anime
 * @returns         an object with all informations to generate the error code
 */
function AnimeNotFound(name: string) {
  const errorManager = new ErrorManager
  return errorManager.notFoundError(
    "Anime",
    name
  )
}

/**
 *  payload definition for POST and PUT methods
 */
export interface AnimeSchema {
  name: string
  type: string
  image: string
  description: string
  owner: string
}

/**
 *  async function to retrieve all waifus
 *  @return a promise with an array of AnimeModel
 */
export const getAllAnimes = async (): Promise<AnimeModel[]> => {

  const animeRepository = getMongoRepository( AnimeModel )
  return animeRepository.find()
}

/**
 *  async function to retrieve all anime by a type name
 *  @return a promise with an array of AnimeModel
 */
export const getAllAnimesByType = async (
request: string
): Promise<AnimeModel[]> => {
  const waifuRepository = getMongoRepository( AnimeModel )
  return waifuRepository.find({ 
    where: {
      type: request 
    }
  })
}

/**
 *  async function to retrieve all animes by an owner name
 *  @param  request name of the owner to find
 *  @return         a promise with an array of AnimeModel
 */
export const getAllAnimesByOwner = async (
  request: string
): Promise<AnimeModel[]> => {
  const animeRepository = getMongoRepository( AnimeModel )
  return animeRepository.find({ 
    where: {
      owner: request 
    }
  })
}

/**
 *  async function to create a new waifu
 *  @param  request AnimeSchema with data
 *  @return         a promise with the created waifu
 */
export const createAnime = async (
  request: AnimeSchema
): Promise<AnimeModel> => {

  const animeRepository = getMongoRepository( AnimeModel )
  const anime = new AnimeModel()
  
  return animeRepository.save({
    ...anime,
    ...request,
  })
}

/**
 *  async function to retrieve one waifu by its name
 *  @param  request     name of the waifu to find
 *  @return             a promise with a WaifuModel
 */
export const getOneAnimeByName = async (
  request: string
): Promise<AnimeModel> => {
  
  const animeRepository = getMongoRepository( AnimeModel )
  let anime = null
  anime = await animeRepository.findOne({ name: request })
    
  if ( !anime )
  {
    throw AnimeNotFound( request ) as Error
  }
  return anime
}

/**
 *  async function to retrieve one anime by a waifu name
 *  @param  request     name of the anime to find
 *  @return             a promise with a WaifuModel
 */
export const getOneAnimeByWaifu = async (
  request: string
): Promise<AnimeModel> => {
  const waifu = await getOneWaifuByName( request )
  return getOneAnimeByName( waifu.anime )   
}

/**
 *  async function to update one anime
 *  @param  request     name of the anime to update
 *  @param  data        a AnimeSchema with modifications
 *  @return             a promise with the updated AnimeModel
 */
export const updateOneAnime = async (
  request: string,
  data: AnimeSchema
): Promise<AnimeModel> => {
  
  const animeRepository = getMongoRepository( AnimeModel )
  let anime = null
  anime = await animeRepository.findOne({ name: request })
    
  if ( !anime )
  {
    throw AnimeNotFound( request ) as Error
  }

  animeRepository.merge( anime, data )
  return animeRepository.save( anime )
}

/**
 *  async function to delete one anime
 *  @param  request     name of the anime to delete
 *  @return             DeleteResult with affected anime
 */
export const deleteOneAnime = async (
  request: string
): Promise<DeleteResult> => {

  const animeRepository = getMongoRepository(AnimeModel)
  let result = null
  result = await animeRepository.delete({ name: request })

  if ( !result.affected || result.affected < 1 )
  {
    throw AnimeNotFound( request ) as Error
  }
  return result
}
