import { DeleteResult, getMongoRepository } from "typeorm"

import { WaifuModel } from "../Models"
import { ErrorManager } from "../Security"


/**
 * generate an error message if the waifu doesn't exists
 * @param name      the name of the waifu
 * @returns         an object with all informations to generate the error code
 */
function WaifuNotFound( name: string ) {
  const errorManager = new ErrorManager
  return errorManager.notFoundError(
    "Waifu",
    name
  )
}

/**
 *  payload definition for POST and PUT methods
 */
export interface WaifuSchema {
  name: string
  age: number
  anime: string
  image: string
  owner: string
}

/**
 *  async function to retrieve all waifus
 *  @return         a promise with an array of WaifuModel
 */
export const getAllWaifus = async (): Promise<WaifuModel[]> => {
  const waifuRepository = getMongoRepository( WaifuModel )
  return waifuRepository.find()
}

/**
 *  async function to retrieve all waifus by an anime name
 *  @param  request name of the anime to find
 *  @return         a promise with an array of WaifuModel
 */
export const getAllWaifusByAnime = async (
  request: string
): Promise<WaifuModel[]> => {
  const waifuRepository = getMongoRepository( WaifuModel )
  return waifuRepository.find({ 
    where: {
      anime: request 
    }
  })
}

/**
 *  async function to retrieve all waifus by an owner name
 *  @param  request name of the owner to find
 *  @return         a promise with an array of WaifuModel
 */
export const getAllWaifusByOwner = async (
  request: string
): Promise<WaifuModel[]> => {
  const waifuRepository = getMongoRepository( WaifuModel )
  return waifuRepository.find({ 
    where: {
      owner: request 
    }
  })
}

/**
 *  async function to create a new waifu
 *  @param  request WaifuSchema with data
 *  @return         a promise with the created waifu
 */
export const createWaifu = async (
  request: WaifuSchema
): Promise<WaifuModel> => {

  const waifuRepository = getMongoRepository( WaifuModel )
  const waifu = new WaifuModel()
  
  return waifuRepository.save({
    ...waifu,
    ...request,
  })
}

/**
 *  async function to retrieve one waifu by its name
 *  @param  request name of the waifu to find
 *  @return         a promise with a WaifuModel
 */
export const getOneWaifuByName = async (
  request: string
): Promise<WaifuModel> => {

  const waifuRepository = getMongoRepository( WaifuModel )
  let waifu = null
  waifu = await waifuRepository.findOne({ name: request })
    
  if ( !waifu )
  {
    throw WaifuNotFound( request ) as Error
  }
  return waifu
}

/**
 *  async function to update one waifu
 *  @param  request name of the waifu to update
 *  @param  data    a WaifuSchema with modifications
 *  @return         a promise with the updated WaifuModel
 */
export const updateOneWaifu = async (
  request: string,
  data: WaifuSchema
): Promise<WaifuModel> => {

  const waifuRepository = getMongoRepository( WaifuModel )
  let waifu = null
  waifu = await waifuRepository.findOne({ name: request })
    
  if ( !waifu )
  {
    throw WaifuNotFound( request ) as Error
  }

  waifuRepository.merge( waifu, data )
  return waifuRepository.save( waifu )
}

/**
 *  async function to delete one waifu
 *  @param  request name of the waifu to delete
 *  @return         DeleteResult with affected waifu
 */
export const deleteOneWaifu = async (
  request: string
): Promise<DeleteResult> => {

  const waifuRepository = getMongoRepository( WaifuModel )
  const result: DeleteResult = await waifuRepository.delete({ name: request })

  if ( !result.affected || result.affected < 1 )
  {
    throw WaifuNotFound( request ) as Error
  }
  return result
}
