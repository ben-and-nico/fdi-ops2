import { ConnectionOptions } from "typeorm"

import {
  WaifuModel,
  AnimeModel,
  TypeModel,
  UserModel
} from "./Models"


/**
 *  Database configuration for mongodb connection
 */
const dbConfig: ConnectionOptions = ({
  type: "mongodb",
  port: Number(process.env["DATABASE_PORT"]) || 27017,
  host: process.env["DATABASE_HOST"] || "127.0.0.1",
  database: process.env["DATABASE_NAME"] || "waifu_db",
  username: process.env["DATABASE_USER"] || "admin",
  password: process.env["DATABASE_PASSWORD"] || "adminpassword",
  entities: [WaifuModel, AnimeModel, TypeModel, UserModel],
  synchronize: true,
  useUnifiedTopology: true
})

export default dbConfig
