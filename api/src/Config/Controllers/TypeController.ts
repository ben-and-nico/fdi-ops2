import { DeleteResult } from "typeorm"
import { Request } from "express"

import { 
  createType,
  getAllTypes,
  getOneTypeByName,
  updateOneType,
  deleteOneType,
  TypeSchema,
  getAllTypesByOwner, 
  getOneTypeByAnime
} from "../Repositories/TypeRepo"
import { TypeModel } from "../Models"
import { AuthManager, ErrorManager, RessourceChecker } from "../Security"


const dbError = new ErrorManager()
const ressourceChecker = new RessourceChecker()
const auth = new AuthManager()

export class TypeController {
  /**
   *  call the Type repo to retrieve all types from database
   *  @return         a promise with an array of TypeModel
   */
  public async retrieve_all(): Promise<TypeModel[]> {
    return getAllTypes()
  }

  /**
   *  call the type repo to find all types created by an user
   *  @param  request   owner's username
   *  @return           a promise with an array of TypeModel
   */
  public async retrieve_all_by_owner(
    request: string
  ): Promise<TypeModel[]> {
    return getAllTypesByOwner(
      request
    )
  }

  /**
   *  call the Type repo to create a brand new Type
   *  @param  body    format the sent body to a TypeSchema
   *  @return         a promise with the TypeModel created
   */
  public async create(
    request: Request
  ): Promise<TypeModel> {
    const body: TypeSchema = request.body
    body.owner = await auth.extractUser(request.headers.authorization)
    return createType( body )
  }

  /**
   *  call the Type repo to find one Type in database
   *  @param  request   Type's name to find by name
   *  @return           a promise with the TypeModel found
   */
  public async retrieve_one_by_name(
    request: string
  ): Promise<TypeModel> {
    return getOneTypeByName(
      request
    )
  }

  /**
   *  call the Type repo to find one Type in database
   *  @param  request   Type's name to find by anime
   *  @return           a promise with the TypeModel found
   */
  public async retrieve_one_by_anime(
    request: string
  ): Promise<TypeModel> {
    return getOneTypeByAnime(
      request
    )
  }

  /**
   *  call the Type repo to update one Type in database
   *  @param  request   Type's name to find for update
   *  @param  body      format the sent body to a TypeSchema
   *  @return           a promise with the TypeModel updated
   */
  public async update(
    request: Request<{name: string}>
  ): Promise<TypeModel> {
    const user = await auth.extractUser(request.headers.authorization)
    const body: TypeSchema = request.body
    const isOwner = await auth.checkOwnership(user, request.params.name, "types")
    if(!isOwner)
    {
      throw dbError.ownershipError("types", request.params.name)
    }
    return updateOneType(
      request.params.name,
      body
    )
  }

  /**
   *  call the Type repo to delete one Type in database
   *  @param  request   Type's name to find for deletion
   *  @return           a promise with the DeletedResult
   */
  public async delete(
    request: Request<{name: string}>
  ): Promise<DeleteResult> {
    const user = await auth.extractUser(request.headers.authorization)
    const isOwner = await auth.checkOwnership(user, request.params.name, "types")
    if(!isOwner)
    {
      throw dbError.ownershipError("types", request.params.name)
    }
    await ressourceChecker.isRessourceHaveChild( "type", request.params.name )
    return deleteOneType(
      request.params.name
    )
  }
}
