import { DeleteResult } from "typeorm"

import { 
  createWaifu,
  getAllWaifus,
  getOneWaifuByName,
  updateOneWaifu,
  deleteOneWaifu,
  getAllWaifusByAnime,
  getAllWaifusByOwner,
  WaifuSchema 
} from "../Repositories/WaifuRepo"
import { WaifuModel } from "../Models"
import { AuthManager, ErrorManager, RessourceChecker } from "../Security"
import { Request } from "express"

const dbError = new ErrorManager()
const ressourceChecker = new RessourceChecker()
const auth = new AuthManager()

export class WaifuController {
  /**
   *  call the waifu repo to retrieve all waifus from database
   *  @return         a promise with 
   */
  public async retrieve_all(): Promise<WaifuModel[]> {
    return getAllWaifus()
  }

  /**
   *  call the waifu repo to find all waifus of a anime in database
   *  @param  request   anime's name to find all waifus in it
   *  @return           a promise with an array of WaifuModel
   */
  public async retrieve_all_by_anime(
    request: string
  ): Promise<WaifuModel[]> {
    await ressourceChecker.isRessourceDefined( "anime", request )
    return getAllWaifusByAnime(
      request
    )
  }

  /**
   *  call the waifu repo to find one waifu in database
   *  @param  request   owner's username
   *  @return           a promise with an array of WaifuModel
   */
   public async retrieve_all_by_owner(
    request: string
  ): Promise<WaifuModel[]> {
    return getAllWaifusByOwner(
      request
    )
  }

  /**
   *  call the waifu repo to create a brand new waifu
   *  @param  body    format the sent body to a WaifuSchema
   *  @return         a promise with the WaifuModel created
   */
  public async create(
    request: Request
  ): Promise<WaifuModel> {
    const body: WaifuSchema = request.body
    body.owner = await auth.extractUser(request.headers.authorization)
    await ressourceChecker.isRessourceDefined( "anime", body.anime )
    return createWaifu( body )
  }

  /**
   *  call the waifu repo to find one waifu in database
   *  @param  request   waifu's name to find
   *  @return           a promise with the WaifuModel found
   */
  public async retrieve_one(
    request: string
  ): Promise<WaifuModel> {
    return getOneWaifuByName(
      request
    )
  }

  /**
   *  call the waifu repo to update one waifu in database
   *  @param  request   waifu's name to find for update
   *  @param  body      format the sent body to a WaifuSchema
   *  @return           a promise with the WaifuModel updated
   */
  public async update(
    request: Request<{name: string}>
  ): Promise<WaifuModel> {
    const body: WaifuSchema = request.body
    const user = await auth.extractUser(request.headers.authorization)
    const isOwner = await auth.checkOwnership(user, request.params.name, "waifus")
    
    if(!isOwner)
    {
      throw dbError.ownershipError("waifu", request.params.name)
    }
    if( body.anime ) {
      await ressourceChecker.isRessourceDefined( "anime", body.anime )
    }

    return updateOneWaifu(
      request.params.name,
      body
    )
  }

  /**
   *  call the waifu repo to delete one waifu in database
   *  @param  request   waifu's name to find for deletion
   *  @return           a promise with the DeletedResult
   */
  public async delete(
    request: Request<{name: string}>
  ): Promise<DeleteResult> {
    const user = await auth.extractUser(request.headers.authorization)
    const isOwner = await auth.checkOwnership(user, request.params.name, "waifus")
    if(!isOwner)
    {
      throw dbError.ownershipError("waifu", request.params.name)
    }
    return deleteOneWaifu(
      request.params.name
    )
  }
}
