import { DeleteResult } from "typeorm"

import { 
  createAnime,
  getAllAnimes,
  getOneAnimeByName,
  updateOneAnime,
  deleteOneAnime,
  AnimeSchema, 
  getOneAnimeByWaifu,
  getAllAnimesByType,
  getAllAnimesByOwner
} from "../Repositories/AnimeRepo"
import { AnimeModel } from "../Models"
import { AuthManager, ErrorManager, RessourceChecker } from "../Security"
import { Request } from "express"

const auth = new AuthManager()
const dbError = new ErrorManager()
const ressourceChecker = new RessourceChecker()

export class AnimeController {
  /**
   *  call the Anime repo to retrieve all animes from database
   *  @return         a promise with an array of AnimeModel
   */
  public async retrieve_all(): Promise<AnimeModel[]> {
    return getAllAnimes()
  }

  /**
   *  call the anime repo to find all animes created by an user
   *  @param  request   owner's username
   *  @return           a promise with an array of AnimeModel
   */
   public async retrieve_all_by_owner(
    request: string
  ): Promise<AnimeModel[]> {
    return getAllAnimesByOwner(
      request
    )
  }

  /**
   *  call the waifu repo to find one waifu in database
   *  @param  request  anime's name where to find all waifus in it
   *  @return           a promise with an array of WaifuModel
   */
  public async retrieve_all_by_type(
    request: string
  ): Promise<AnimeModel[]> {
    await ressourceChecker.isRessourceDefined( "type", request )
    return getAllAnimesByType(
      request
    )
  }


  /**
   *  call the Anime repo to create a brand new Anime
   *  @param  body    format the sent body to a AnimeSchema
   *  @return         a promise with the AnimeModel created
   */
  public async create(
    request: Request
  ): Promise<AnimeModel> {
    const body: AnimeSchema = request.body
    body.owner = await auth.extractUser(request.headers.authorization)
    await ressourceChecker.isRessourceDefined( "type", body.type )
    return createAnime( body )
  }


  /**
   *  call the Anime repo to find one Anime in database
   *  @param  request   Anime's name to find by name
   *  @return           a promise with the AnimeModel found
   */
  public async retrieve_one_by_name(
    request: string
  ): Promise<AnimeModel> {
    return getOneAnimeByName(
      request
    )
  }

  /**
   *  call the Anime repo to find one Anime in database
   *  @param  request   Anime's name to find by waifu
   *  @return           a promise with the AnimeModel found
   */
  public async retrieve_one_by_waifu(
    request: string
  ): Promise<AnimeModel> {
    return getOneAnimeByWaifu(
      request
    )
  }

  /**
   *  call the Anime repo to update one Anime in database
   *  @param  request   Anime's name to find for update
   *  @param  body      format the sent body to a AnimeSchema
   *  @return           a promise with the AnimeModel updated
   */
  public async update(
    request: Request<{name: string}>
  ): Promise<AnimeModel> {
    const body: AnimeSchema = request.body
    const user = await auth.extractUser(request.headers.authorization)
    const isOwner = await auth.checkOwnership(user, request.params.name, "animes")
    if(!isOwner)
    {
      throw dbError.ownershipError("animes", request.params.name)
    }
    if( body.type ) {
      await ressourceChecker.isRessourceDefined( "type", body.type )
    }
    return updateOneAnime(
      request.params.name,
      body
    )
  }

  /**
   *  call the Anime repo to delete one Anime in database
   *  @param  request   Anime's name to find for deletion
   *  @return           a promise with the DeletedResult
   */
  public async delete(
    request: Request<{name: string}>
  ): Promise<DeleteResult> {
    const user = await auth.extractUser(request.headers.authorization)
    const isOwner = await auth.checkOwnership(user, request.params.name, "animes")
    if(!isOwner)
    {
      throw dbError.ownershipError("animes", request.params.name)
    }
    await ressourceChecker.isRessourceHaveChild( "anime", request.params.name )
    return deleteOneAnime(
      request.params.name
    )
  }
}

