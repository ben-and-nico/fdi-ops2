import CryptoJS from "crypto-js"
import * as jwt from "jsonwebtoken"

import { ErrorManager } from "."
import Secret from "./Secret"
import { 
  getOneUserByUsername, 
  UserSchema 
} from "../Repositories/UserRepo"
import { 
  AnimeController, 
  TypeController,
  WaifuController 
} from "../Controllers"
import { 
  UserModel, 
  WaifuModel, 
  TypeModel, 
  AnimeModel 
} from "../Models"

interface tokenPayload {
  username: string,
  iat: number,
  exp: number
}

interface ownerRessources {
  waifus: WaifuModel[],
  animes: AnimeModel[],
  types: TypeModel[]
}

export interface tokenInformations {
  token: string,
  expiresIn: string
}

export default class AuthManager {

  /**
   *  Encrypt a password passed as input
   *  @param  input   the password to encrypt
   *  @returns        the encrypted password
   */
  public encryptPassword(input: string): string {
    return CryptoJS.SHA3(input).toString(CryptoJS.enc.Base64)
  }

  /**
   *  Check if credentials are valid
   *  @param  request   the request with username & password
   *  @param  toCompare the user to compare with
   *  @returns          the JWT Token
   */
  public checkCredentials(request: UserSchema, toCompare: UserModel): tokenInformations {
    if(request.password != toCompare.password) {
      const dbError = new ErrorManager()
      throw dbError.wrongCredentialsError(request.username) as Error
    }

    const token: tokenInformations = {
      token: jwt.sign({ username: request.username }, Secret.jwtSecret, { expiresIn: "1h" }),
      expiresIn: "1h"
    }

    return token
  }

  /**
   *  Check if the token and the user are valid
   *  @param token      the JWT Token retrieved from headers before
   *  @returns          the username of the user logged in
   */
  public async checkToken(token: string): Promise<string> {
    try {
      const verification = <tokenPayload>jwt.verify(token, Secret.jwtSecret)
      const user: UserModel = await getOneUserByUsername(verification.username)
      return user.username
    } catch (error) {
      throw error as Error
    }
  }

  /**
   *  Extract the username from a JWT Token
   *  @param token      the JWT Token retrieved from headers before 
   *  @returns          the checkToken methods (which returns the username after checks)
   */
  public async extractUser(token: string | undefined): Promise<string> {
    if(!token) {
      const dbError = new ErrorManager()
      throw dbError.missingToken() as Error
    } else if(token.split(" ").length == 1) {
      return this.checkToken(token)
    } 
    return this.checkToken(token.split(" ")[1])
  }

  /**
   *  Check if a user is the owner of a specified ressource
   *  @param username       the username of the user
   *  @param ressource      the value of the ressource to find
   *  @param ressource_type the type of the ressource to find
   *  @returns              a boolean (true if the user is the owner)
   */
  public async checkOwnership(
    username: string, 
    ressource: string, 
    ressource_type: string
  ): Promise<boolean> {
    if (ressource_type == "types")
    {
      const typeController = new TypeController()
      const type = await typeController.retrieve_one_by_name(ressource)
      return (type.owner == username)
    } else if (ressource_type == "animes")
    {
      const animeController = new AnimeController()
      const anime = await animeController.retrieve_one_by_name(ressource)
      return (anime.owner == username)
    }
    const waifuController = new WaifuController()
    const waifu = await waifuController.retrieve_one(ressource)
    return (waifu.owner == username)
  }

  /**
   *  Check if a user is still associated to any ressources
   *  @param username   the username of the user
   *  @returns          a boolean (true if the user still owner)
   */
  public async isStillOwner(username: string): Promise<boolean> {
    const typeController = new TypeController()
    const animeController = new AnimeController()
    const waifuController = new WaifuController()
    
    const ressources: ownerRessources = {
      types: await typeController.retrieve_all_by_owner(username),
      animes: await animeController.retrieve_all_by_owner(username),
      waifus: await waifuController.retrieve_all_by_owner(username)
    }
    if(ressources.types.length > 0) 
    {
      return true
    } else if (ressources.animes.length > 0)
    {
      return true
    } else if (ressources.waifus.length > 0) 
    {
      return true
    }
    return false
  }

  /**
   *  Check if the user provided in the token is the same you want to delete
   *  @param username   the username of the user
   *  @param token      the token of the req
   *  @returns          a boolean (true if the user is the owner)
   */
  public async isHimself(username: string, token: string | undefined): Promise<boolean> {
    const user = await this.extractUser(token)
    return (username == user)
  }
}
