import { Router } from "express"

import WaifuRoutes from "./WaifuRoutes"
import AnimeRoutes from "./AnimeRoutes"
import TypeRoutes from "./TypeRoutes"
import UserRoutes from "./UserRoutes"
import HealthcheckRoutes from "./HealthcheckRoutes"


const router = Router()

router.use( "/waifus", WaifuRoutes )
router.use( "/animes", AnimeRoutes )
router.use( "/types", TypeRoutes )
router.use( "/users", UserRoutes )
router.use( "/healthcheck", HealthcheckRoutes )

export default router
