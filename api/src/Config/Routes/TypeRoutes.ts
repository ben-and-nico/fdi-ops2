import express from "express"
import { TypeController } from "../Controllers"
import { ErrorManager } from "../Security"

const router = express.Router()
const typeController = new TypeController()
const errorManager = new ErrorManager()

/**
 *  GET all type async route
 *  @return     status + response
 */
router.get("/", async (_, res) => {
  try {
    const response = await typeController.retrieve_all()
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})
    
  
/**
 *  GET one type by anime name async route
 *  @return     status + response
 */
router.get("/anime/:name", async (req, res) => {
  try {
    const response = await typeController.retrieve_one_by_anime(req.params.name)
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})
  
  
/**
 *  POST new type async route
 *  @return     status + response
 */
router.post("/", async (req, res) => {
  try {
    const response = await typeController.create(req)
    return res.status(201).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})
  
  
/**
 *  GET one type by name async route
 *  @return     status + response
 */
router.get("/:name", async (req, res) => {
  try {
    const response = await typeController.retrieve_one_by_name(req.params.name)
    return res.status(200).send(response)  
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  PUT one type by name async route
 *  @return     status + response
 */
router.put("/:name", async (req, res) => {
  try {
    const response = await typeController.update(req)
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  DELETE one type by name async route
 *  @return     status + response
 */
router.delete("/:name", async (req, res) => {
  try {
    await typeController.delete(req)
    return res.status(204).send()
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})

export default router
