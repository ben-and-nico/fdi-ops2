import express from "express"
import { WaifuController } from "../Controllers"
import { ErrorManager } from "../Security"

const router = express.Router()
const waifuController = new WaifuController()
const errorManager = new ErrorManager()


/**
 *  GET all waifu async route
 *  @return     status + response
 */
router.get("/", async (_, res) => {
  try {
    const response = await waifuController.retrieve_all()
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  GET all waifu async route
 *  @return     status + response
 */
 router.get("/anime/:name", async (req, res) => {
  try {
    const response = await waifuController.retrieve_all_by_anime(req.params.name)
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  POST new waifu async route
 *  @return     status + response
 */
router.post("/", async (req, res) => {
  try {
    const response = await waifuController.create(req)
    return res.status(201).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  GET one waifu by name async route
 *  @return     status + response
 */
router.get("/:name", async (req, res) => {
  try {
    const response = await waifuController.retrieve_one(req.params.name)
    return res.status(200).send(response)  
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  PUT one waifu by name async route
 *  @return     status + response
 */
router.put("/:name", async (req, res) => {
  try {
    const response = await waifuController.update(req)
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  DELETE one waifu by name async route
 *  @return     status + response
 */
router.delete("/:name", async (req, res) => {
  try {
    await waifuController.delete(req)
    return res.status(204).send()
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


export default router
