import "reflect-metadata"
import express from "express"
import cors from "cors"

import Router from "./Config/Routes"
import { winstonLogger, morganLogger } from "./Config/Loggers"

const app = express()

const origins = ["*"]
const options: cors.CorsOptions = {
  origin: origins
}

app.use(cors(options))
winstonLogger.info("CORS enabled on all routes.")

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
winstonLogger.info("Express is using JSON as default return type.")

app.use(morganLogger)
winstonLogger.info("Access logger activated.")

app.use(Router)
winstonLogger.info("Routes correctl added to this application.")

export default app
