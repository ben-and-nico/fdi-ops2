import request from "supertest"
import { Connection, createConnection } from "typeorm"
import dbConfig from "../src/Config/database"

import app from "../src/express"


let connection: Connection
let token: string

beforeAll(async () => {
  connection = await createConnection(dbConfig)
})

afterAll(async () => {
  await connection.close()
})

describe("GET /healthcheck", () => {
  it("Route Healthcheck Test", async () => {
    const result = await request(app).get("/healthcheck")
    expect(result.statusCode).toEqual(204)
  })
})

describe("User creation and login", () => {
  it("POST new user", async () => {
    const result = await request(app)
      .post("/users")
      .send({
        username: "jest",
        password: "jest"
      })
    expect(result.statusCode).toEqual(201)
  })
  it("LOGIN the new user", async () => {
    const result = await request(app)
      .post("/users/login")
      .send({
        username: "jest",
        password: "jest"
      })
    token = result.body.token
    expect(result.statusCode).toEqual(200)
  })
})

describe("Create ressources", () => {
  it("POST new type", async () => {
    const result = await request(app)
      .post("/types")
      .set("Authorization", "bearer " + token)
      .send({
        name: "jest_type",
        image: "none",
        description: "none",
      })
    expect(result.statusCode).toEqual(201)
  })
  it("POST new anime", async () => {
    const result = await request(app)
      .post("/animes")
      .set("Authorization", "bearer " + token)
      .send({
        name: "jest_anime",
        type: "jest_type",
        image: "none",
        description: "none"
      })
    expect(result.statusCode).toEqual(201)
  })
  it("POST new waifu", async () => {
    const result = await request(app)
      .post("/waifus")
      .set("Authorization", "bearer " + token)
      .send({
        name: "jest_waifu",
        image: "none",
        age: 18,
        anime: "jest_anime"
      })
    expect(result.statusCode).toEqual(201)
  })
})

describe("UPDATE ressources", () => {
  it("UPDATE type", async () => {
    const result = await request(app)
      .put("/types/jest_type")
      .set("Authorization", "bearer " + token)
      .send({
        image: "updated"
      })
    expect(result.statusCode).toEqual(200)
  })
  it("UPDATE anime", async () => {
    const result = await request(app)
      .put("/animes/jest_anime")
      .set("Authorization", "bearer " + token)
      .send({
        image: "updated"
      })
    expect(result.statusCode).toEqual(200)
  })
  it("UPDATE waifu", async () => {
    const result = await request(app)
      .put("/waifus/jest_waifu")
      .set("Authorization", "bearer " + token)
      .send({
        image: "updated"
      })
    expect(result.statusCode).toEqual(200)
  })
})

describe("GET ressources", () => {
  it("GET types", async () => {
    const result = await request(app)
      .get("/types")
    expect(result.statusCode).toEqual(200)
  })
  it("GET animes", async () => {
    const result = await request(app)
      .get("/animes")
    expect(result.statusCode).toEqual(200)
  })
  it("GET waifus", async () => {
    const result = await request(app)
      .get("/waifus")
    expect(result.statusCode).toEqual(200)
  })

  it("GET one type by anime name", async () => {
    const result = await request(app)
      .get("/types/anime/jest_anime")
    expect(result.statusCode).toEqual(200)
  })
  it("GET one type by name", async () => {
    const result = await request(app)
      .get("/types/jest_type")
    expect(result.statusCode).toEqual(200)
  })

  it("GET one anime by waifu name", async () => {
    const result = await request(app)
      .get("/animes/waifu/jest_waifu")
    expect(result.statusCode).toEqual(200)
  })
  it("GET one anime by name", async () => {
    const result = await request(app)
      .get("/animes/jest_anime")
    expect(result.statusCode).toEqual(200)
  })

  it("GET all waifus by anime name", async () => {
    const result = await request(app)
      .get("/waifus/anime/jest_anime")
    expect(result.statusCode).toEqual(200)
  })
  it("GET one waifu by name", async () => {
    const result = await request(app)
      .get("/waifus/jest_waifu")
    expect(result.statusCode).toEqual(200)
  })
})


describe("DELETE ressources", () => {
  it("DELETE waifu", async () => {
    const result = await request(app)
      .delete("/waifus/jest_waifu")
      .set("Authorization", "bearer " + token)
    expect(result.statusCode).toEqual(204)
  })
  it("DELETE anime", async () => {
    const result = await request(app)
      .delete("/animes/jest_anime")
      .set("Authorization", "bearer " + token)
    expect(result.statusCode).toEqual(204)
  })
  it("DELETE type", async () => {
    const result = await request(app)
      .delete("/types/jest_type")
      .set("Authorization", "bearer " + token)
    expect(result.statusCode).toEqual(204)
  })
})

describe("User deletion", () => {
  it("DELETE the new user", async () => {
    const result = await request(app)
      .delete("/users/jest")
      .set("Authorization", "bearer " + token)
    expect(result.statusCode).toEqual(204)
  })
})
