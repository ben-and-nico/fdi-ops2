import ErrorManager from "../src/Config/Security/DatabaseError"

const test_manager: ErrorManager = new ErrorManager()

interface ErrorMsg {
  name: string,
  message: string,
  type: string,
  value: string | undefined
}

describe("Errors generators tests", () => {
  it("Should create a NotFoundError", () => {
    const error: ErrorMsg = test_manager.notFoundError("type", "value")
    expect(error.name).toEqual("NotFoundError")
    expect(error.type).toEqual("type")
    expect(error.value).toEqual("value")
  })

  it("Should create a QueryFailedError", () => {
    const error: ErrorMsg = test_manager.badRequestError("type", "value")
    expect(error.name).toEqual("QueryFailedError")
    expect(error.type).toEqual("type")
    expect(error.value).toEqual("value")
  })

  it("Should create a QueryFailedError 2", () => {
    const error: ErrorMsg = test_manager.haveChildError("type", "value")
    expect(error.name).toEqual("QueryFailedError")
    expect(error.type).toEqual("type")
    expect(error.value).toEqual("value")
  })

  it("Should create a JsonWebTokenError", () => {
    const error: ErrorMsg = test_manager.missingToken()
    expect(error.name).toEqual("JsonWebTokenError")
    expect(error.type).toEqual("JsonWebToken")
    expect(error.value).toEqual(undefined)
  })

  it("Should create a CredentialsError", () => {
    const error: ErrorMsg = test_manager.wrongCredentialsError("value")
    expect(error.name).toEqual("CredentialsError")
    expect(error.type).toEqual("user")
    expect(error.value).toEqual("value")
  })

  it("Should create a OwnershipError", () => {
    const error: ErrorMsg = test_manager.ownershipError("type", "value")
    expect(error.name).toEqual("OwnershipError")
    expect(error.type).toEqual("type")
    expect(error.value).toEqual("value")
  })

  it("Should create a OwnershipError 2", () => {
    const error: ErrorMsg = test_manager.stillOwnerError("value")
    expect(error.name).toEqual("OwnershipError")
    expect(error.type).toEqual("user")
    expect(error.value).toEqual("value")
  })
})

describe("Error codes tests", () => {
  const error: ErrorMsg = {
    name: "name",
    message: "message",
    type: "type",
    value: "value"
  }

  it("Should return 400", () => {
    error.name = "QueryFailedError"
    const code: number = test_manager.readError(error)
    expect(code).toEqual(400)
  })

  it("Should return 400", () => {
    error.name = "BulkWriteError"
    const code: number = test_manager.readError(error)
    expect(code).toEqual(400)
  })

  it("Should return 400", () => {
    error.name = "TokenExpiredError"
    const code: number = test_manager.readError(error)
    expect(code).toEqual(400)
  })

  it("Should return 401", () => {
    error.name = "CredentialsError"
    const code: number = test_manager.readError(error)
    expect(code).toEqual(401)
  })

  it("Should return 403", () => {
    error.name = "JsonWebTokenError"
    const code: number = test_manager.readError(error)
    expect(code).toEqual(403)
  })

  it("Should return 403", () => {
    error.name = "NotBeforeError"
    const code: number = test_manager.readError(error)
    expect(code).toEqual(403)
  })

  it("Should return 403", () => {
    error.name = "OwnershipError"
    const code: number = test_manager.readError(error)
    expect(code).toEqual(403)
  })

  it("Should return 404", () => {
    error.name = "NotFoundError"
    const code: number = test_manager.readError(error)
    expect(code).toEqual(404)
  })
})