set -e

mongo <<EOF
const dbname = "$DATABASE_NAME" || "waifu_db";
const username = "$DATABASE_USER" || "admin";
const password = "$DATABASE_PASSWORD" || "adminpassword";

db = db.getSiblingDB(dbname);
db.createUser(
  {
    user: username,
    pwd: password,
    roles: [{ role: "readWrite", db: dbname }],
  },
);
db.createCollection("waifus");
db.createCollection("animes");
db.createCollection("types");
db.createCollection("users");

EOF
